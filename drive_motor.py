import logging
import time
import sys
import traceback
import math
import odrive
from odrive.enums import *
from odrive.utils import dump_errors
import argparse 

gearbox_rr = 42.9
pulley_rr = 4.655

run_time = 1 # minutes
class motor_handler:
    def __init__(self):
        self.pin = 18
        #self.pin_state = GPIO.LOW
        #GPIO.setmode(GPIO.BCM)
        #GPIO.setup(self.pin, GPIO.OUT)
        #odrv0.axis0.controller.config.control_mode = ControlMode.VELOCITY_CONTROL
        #odrv0.axis0.controller.input_vel = 1
        #odrv0.axis0.requested_state=8 # Velocity control
        #odrv0.axis0.requested_state=1 # idle
        #odrv0.ibus * odrv0.vbus_vltage check motor powero

        self.odrv = odrive.find_any()
        self.motor_on_time = 0
        self.motor_on_time_start = time.time()
        self.motor_on = False


    def get_motor_on_time(self):
        if motor_on:
            return time.time() - self.motor_on_time_start + self.motor_on_time
        else:
            return self.motor_on_time
    """
    def change_direction(self):
        # Drive relay high
        if self.pin_state == GPIO.LOW:
            print("Turning Valve On")
            self.timestart = time.time()
            GPIO.output(self.pin, GPIO.HIGH)
            self.pin_state = GPIO.HIGH
        else:
            print("Valve on for: {}".format(time.time() - self.timestart))
            GPIO.output(self.pin, GPIO.LOW)
            GPIO.cleanup()
            self.pin_state = GPIO.LOW
    """
    def cleanup(self):
        #GPIO.cleanup()
        pass

    def drive_motor_forward(self, rpm = 1.0):
        # rpm conversion to tps
        #tps = rpm
        tps = (rpm * (gearbox_rr * pulley_rr)) / 60

        # Driving motor
        self.odrv.axis0.controller.config.control_mode = ControlMode.VELOCITY_CONTROL
        self.odrv.axis0.controller.config.vel_ramp_rate = 10.0                           # 0.5 for platform
        self.odrv.axis0.controller.config.input_mode = INPUT_MODE_VEL_RAMP
        self.odrv.axis0.controller.input_vel = tps
        self.odrv.axis0.requested_state=8

        # Making sure it has the right safety config here too
        self.odrv.axis0.motor.config.current_lim = 30.0
        self.odrv.axis0.controller.config.vel_limit = 5.0
        self.odrv.axis0.controller.config.spinout_electrical_power_threshold = 100.0
        self.odrv.axis0.controller.config.spinout_mechanical_power_threshold = -100.0

        self.motor_on = True
        self.motor_on_time_start = time.time()

    def vel_changer(self, tps=10):
        self.odrv.axis0.controller.input_vel = tps

    def position_control(self, angle = 15.0, total_tm=0.0, pause_ts=0.0, rpm=1.0):
        global run_time
        rpm_in_tps = (rpm * (gearbox_rr * pulley_rr)) / 60
        aux_angle_to_time = 360 / angle

        self.odrv.axis0.controller.config.control_mode = ControlMode.VELOCITY_CONTROL
        self.odrv.axis0.controller.config.vel_ramp_rate = 20  # HIGH ENOUGH VALUE FOR MORE ACCURATE POSITION CONTROL
        self.odrv.axis0.controller.config.input_mode = INPUT_MODE_VEL_RAMP
        self.odrv.axis0.controller.input_vel = rpm_in_tps
        self.odrv.axis0.requested_state = 8
        # Making sure it has the right safety config here too
        self.odrv.axis0.motor.config.current_lim = 30.0
        self.odrv.axis0.controller.config.vel_limit = 5.0
        self.odrv.axis0.controller.config.spinout_electrical_power_threshold = 100.0
        self.odrv.axis0.controller.config.spinout_mechanical_power_threshold = -100.0

        if pause_ts != 0.0:
            run_time = total_tm * 60
            print("runtime is: ", run_time)
            interval_ts = (60 / aux_angle_to_time)

            self.motor_on = True
            abs_start_time = time.time()
            self.motor_on_time_start = time.time()

            try:
                while time.time() - abs_start_time < run_time:
                    #print(time.time() - self.motor_on_time_start)
                    if time.time() - self.motor_on_time_start >= interval_ts:
                        self.odrv.axis0.controller.input_vel = 0.0 # Stopping the motor
                        print("Pausing for: ", pause_ts, " secs")
                        time.sleep(pause_ts)
                        self.odrv.axis0.controller.input_vel = rpm_in_tps
                        self.motor_on_time_start = time.time()
                print("DONE")
            except KeyboardInterrupt:
                pass
            run_time = 0.0

        else:
            self.motor_on = True
            self.motor_on_time_start = time.time()

            run_time = (60/aux_angle_to_time)/60 # minutes
            print("The running time for angle: ",angle,  "is: ", run_time * 60, "  secs")
            """
            try:
                time.sleep(run_time)
            except KeyboardInterrupt or Exception as e:
                logging.error(traceback.format_exc())
            """

    def watchdog_checker(self, timeout=0):
        self.odrv.axis0.config.watchdog_timeout = timeout

    def watchdog_feeder(self):
        self.odrv.axis0.config.watchdog_feed()

    def show_errors(self, show=False):
        dump_errors(self.odrv)

    def idle_motor(self):
        self.odrv.axis0.controller.input_vel = 0
        self.odrv.axis0.requested_state=1 # idle
        self.motor_on = False
        self.motor_on_time = time.time() - self.motor_on_time_start + self.motor_on_time

    def get_motor_power(self, show=False):
        self.i = self.odrv.ibus
        self.v = self.odrv.vbus_voltage
        self.p = self.i * self.v
        self.speed = self.odrv.axis0.pos_vel_mapper.vel*60
        # Getting torque
        t = abs(((60 * 1 / (2 * math.pi)) * self.p) / self.speed)

        if show:
            print("MOTOR - POWER: {:.2f} W | CURRENT: {:.2f} A | VOLTAGE: {:.2f} V | SPEED: {:.2f} RPM | TEMP: {:.2f} | TORQUE: {:.2f} NM".format(
                self.p, self.i, self.v, self.speed/(gearbox_rr*pulley_rr), self.odrv.axis0.motor.motor_thermistor.temperature, t
            ))

    def get_motor_temp(self, show=False):
        self.temp = self.odrv.axis0.motor.motor_thermistor.temperature
        if show:
            print("Motor Temp: {} C".format(self.temp))

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--mode")
    parser.add_argument("--rpm")
    parser.add_argument("--angle")
    parser.add_argument("--pause")     # pause time in secs
    parser.add_argument("--time")
    parser.add_argument("--dir")
    args=parser.parse_args()

    if args.time == None:
        run_time = math.inf
    else:
        run_time = float(args.time)

    if args.rpm == None:
        rpm = 0.0
    else:
        if args.dir == "cw": # CW is negative number
            rpm = abs(float(args.rpm))
            rpm = rpm - 2 * rpm
        else:               # CC is positive number
            rpm = abs(float(args.rpm))
    motor_object = motor_handler()

    if args.mode == "position":
        if args.pause == None:
            pause = 0.0
        else:
            pause = float(args.pause)
        if args.angle == None:
            angle = 0.0
        else:
            print("ANGLE: ", args.angle)
            angle = float(args.angle)

        motor_object.position_control(float(args.angle), total_tm=run_time, pause_ts=pause, rpm=rpm)
    elif args.mode == "speed":
        motor_object.drive_motor_forward(rpm)
    else:
        print("NO MODE SELECTED. PLEASE SELECT speed or position")
        run_time = 0.0
    timestart = time.time()
    try:
        motor_object.watchdog_checker()
        while time.time() - timestart < run_time * 60:
            #motor_object.watchdog_feeder()
            motor_object.get_motor_power(show=True)

    except KeyboardInterrupt:
        pass

    motor_object.idle_motor()
